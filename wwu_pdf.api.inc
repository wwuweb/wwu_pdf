<?php

/**
 * @file
 * WWU PDF API.
 */

use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Alter settings for a PDF before it is instantiated.
 *
 * @param Options $options
 *   Dompdf options object.
 */
function hook_dompdf_options_alter(Options &$options) {
  $options->set('isPhpEnabled', TRUE);
}

/**
 * Alter a PDF after it has been instantiated but before it has been rendered.
 *
 * @param Dompdf $pdf
 *   Dompdf PDF object.
 */
function hook_dompdf_alter(Dompdf &$pdf) {
  $pdf->setPaper('A4', 'landscape');
}

/**
 * Alter the canvas of a PDF after it has been renderd.
 *
 * This is of particular use for adding executable PHP page scripts to the PDF.
 *
 * @param Canvas $canvas
 *   Dompdf Canvas object.
 * @param $node
 *   An entity metadata wrapper object for the node to be converted to PDF.
 */
function hook_dompdf_canvas_alter(Canvas &$canvas, &$node) {
  $canvas->page_text(0, 0, $node_wrapper->label(), 'Helvetica', 10);
}
