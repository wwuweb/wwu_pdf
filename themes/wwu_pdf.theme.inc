<?php

/**
 * @file
 * WWU PDF theme file.
 */

/**
 * PDF Styles theme function.
 */
function theme_wwu_pdf_styles(&$variables) {
  $output = "<style>\n";

  foreach ($variables['imports'] as $import) {
    $output .= "@import url('" . check_plain($import) . "');\n";
  }

  return $output . '</style>';
}
