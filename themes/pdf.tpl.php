<?php

/**
 * @file
 * WWU PDF Tempalte.
 */
?>
<!DOCTYPE html>
<html>
  <head>
    <title><?php print render($title); ?></title>
    <?php print $style; ?>
  </head>
  <body>
    <?php print render($content); ?>
  </body>
</html>
