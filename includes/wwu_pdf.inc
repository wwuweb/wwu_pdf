<?php

/**
 * @file
 * Utility functions for WWU PDF module.
 *
 * This file contains internal functions that should not be used externally to
 * the module.
 */

use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Get an instance of the Dompdf class.
 */
function _wwu_pdf_dompdf() {
  libraries_load('dompdf');

  $options = new Options();

  drupal_alter('dompdf_options', $options);

  $pdf = new Dompdf($options);

  drupal_alter('dompdf', $pdf);

  return $pdf;
}
